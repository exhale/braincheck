package main

import (
	"github.com/bxcodec/faker/v3"
	"time"
)

type timeoutProvider struct{}

func NewTimeoutProvider() WashProvider {
	return &timeoutProvider{}
}

func (p *timeoutProvider) List() ([]*CarWash, error) {

	time.Sleep(time.Second * 11)

	return []*CarWash{
		{
			Address:   faker.FirstName(),
			Latitude:  faker.LastName(),
			Longitude: faker.Email(),
			OpenHours: faker.Sentence(),
		},
	}, nil
}
