module braincheck

go 1.16

require (
	github.com/bxcodec/faker/v3 v3.6.0
	github.com/davecgh/go-spew v1.1.1
)
