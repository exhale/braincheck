package main

import "github.com/davecgh/go-spew/spew"

type CarWash struct {
	Address   string
	Latitude  string
	Longitude string
	OpenHours string
}

type WashProvider interface {
	List() ([]*CarWash, error)
}

/*
Имея договоренности с партнерами мойками, нам необходимо опрашивать их для получения списка активных точек
куда могут приехать пользователи и помыть свой автомобиль.
Данный алгоритм выполняется каждый раз когда пользователь выбирает у себя в приложении экран "Список моек на карте"

Взгляните на алгоритм опроса и формирования списка моек, а так же на различные реализации WashProvider в файлах *_provider.go

Расскажите о недостатках и проблемах данного кода, поясните как можно его улучшить, и как можно покрыть его тестами
*/

func main() {

	firstProvider := NewProvider()
	secondProvider := NewFailedProvider()
	thirdProvider := NewTimeoutProvider()

	providers := []WashProvider{firstProvider, secondProvider, thirdProvider}

	var washes []*CarWash
	for _, w := range providers {
		list, err := w.List()
		if err != nil {
			panic(err)
		}

		washes = append(washes, list...)
	}

	spew.Dump(washes)
}
