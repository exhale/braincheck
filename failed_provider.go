package main

import "errors"

type failedProvider struct{}

func NewFailedProvider() WashProvider {
	return &failedProvider{}
}

func (p *failedProvider) List() ([]*CarWash, error) {
	return nil, errors.New("failed ;(")
}
