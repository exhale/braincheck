package main

import (
	"github.com/bxcodec/faker/v3"
)

type provider struct {}

func NewProvider() WashProvider {
	return &provider{}
}

func (p *provider) List() ([]*CarWash, error) {
	return []*CarWash{
		{
			Address:   faker.FirstName(),
			Latitude:  faker.LastName(),
			Longitude: faker.Email(),
			OpenHours: faker.Sentence(),
		},
	}, nil
}
